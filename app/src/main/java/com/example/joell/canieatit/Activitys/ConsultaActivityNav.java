package com.example.joell.canieatit.Activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.joell.canieatit.GestorListView.EntityListProductos;
import com.example.joell.canieatit.GestorListView.ProductosListAdapter;
import com.example.joell.canieatit.R;
import com.example.joell.canieatit.classes.Principal;

import java.util.ArrayList;

public class ConsultaActivityNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {

    private SQLiteDatabase mydatabase;

    Principal principal = null;
    private static final String TAG = "MainActivity";


    ArrayList<EntityListProductos> listItemsProductos = new ArrayList<>();
    SharedPreferences sf;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Consultar Productos");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        CrearBBDD();
        buscarAll();
        InicialitzarListView();

        ImageButton btnBuscar = (ImageButton) findViewById(R.id.imageButton);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listItemsProductos.clear();
                ConsultaBuscar();
                InicialitzarListView();
            }
        });
    }

    private void CrearBBDD(){
        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);


    }

    private void InicialitzarListView(){
        //INICIALITZAR I OMPLIR LISTVIEW
        Log.d(TAG, "onCreate: Started.");
        ListView mListView = (ListView) findViewById(R.id.LV_Llista_Productes);

        ProductosListAdapter adapter = new ProductosListAdapter(this, R.layout.item_productos, listItemsProductos);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
    }

    private void buscarAll(){
        //OBRIR BBDD
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);

        // SELECT PER OBTINDRE TOTS ELS PRODUCTES
        String sqlQuery = "SELECT * FROM productos WHERE _id IN (SELECT idProducto FROM union_ WHERE idAlergeno IN (SELECT _id FROM alergenos_user WHERE tiene = 'false'));";
        Cursor resultSet = this.mydatabase.rawQuery(sqlQuery, null);
        resultSet.moveToFirst(); // Movem el cursor al primer resultat

        String str;
        try{
            str = resultSet.getString(2);
        }catch (Exception e){
            str = "";
        }

        if(!str.equals("")) {
            do {
                listItemsProductos.add(new EntityListProductos(resultSet.getString(2), resultSet.getInt(0)));

            }while (resultSet.moveToNext()); //Mentres tinguem algun resultat, torna a executar el codi
        }else{
            Toast.makeText(this,"No hay datos para este producto", Toast.LENGTH_SHORT).show();
            listItemsProductos.add(new EntityListProductos("No tenemos datos para este producto",-1)); // TODO Potser aquest -1 genera error
        }

    }

    private void ConsultaBuscar(){
        EditText ETBuscar = findViewById(R.id.ET_Buscar);
        String productos = ETBuscar.getText().toString();

        //BUSQUEM EL PRODUCTE A LA BBDD
        String sqlQuery = "SELECT * FROM productos WHERE nombre LIKE '%"+productos+"%' AND _id IN (SELECT idProducto FROM union_ WHERE idAlergeno IN (SELECT _id FROM alergenos_user WHERE tiene = 'false'));";

        /*

        SELECT *
                FROM   productos
                WHERE  nombre LIKE '%producte%'
                       AND _id IN (SELECT idproducto
                                   FROM   union_
                                   WHERE  idalergeno IN (SELECT _id
                                                         FROM   alergenos_user
                                                         WHERE  tiene = 'false'))  ;
         */

        Cursor resultSet = this.mydatabase.rawQuery(sqlQuery, null);
        resultSet.moveToFirst(); // Movem el cursor al primer resultat

        String str;
        try{
            str = resultSet.getString(2);
        }catch (Exception e){
            str = "";
        }

        if(!str.equals("")) {
            do {
                listItemsProductos.add(new EntityListProductos(resultSet.getString(2), resultSet.getInt(0)));

            }while (resultSet.moveToNext()); //Mentres tinguem algun resultat, torna a executar el codi
        }else{
            Toast.makeText(this,"No hay datos para este producto", Toast.LENGTH_SHORT).show();
            listItemsProductos.add(new EntityListProductos("No tenemos datos para este producto",-1)); // TODO Potser aquest -1 genera error
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.consulta_activity_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_consultar_consulta) {
            Intent ChangeActivity = new Intent(getApplicationContext(), ConsultaActivityNav.class);
            startActivity(ChangeActivity);
        } else if (id == R.id.nav_perfil_consulta) {
            Intent ChangeActivity = new Intent(getApplicationContext(), PerfilActivityNav.class);
            startActivity(ChangeActivity);
        }else if ( id == R.id.nav_marcas_consulta){
            Intent ChangeActivity = new Intent(getApplicationContext(), MarcaActivityNav.class);
            startActivity(ChangeActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EntityListProductos item = (EntityListProductos) parent.getItemAtPosition(position);
        int Producto = item.getId();

        //Enviem la ID del producte per reutilitzar-la a la següent activity
        sf=PreferenceManager.getDefaultSharedPreferences(ConsultaActivityNav.this);
        SharedPreferences.Editor editor = sf.edit();
        editor.putInt("Producto", Producto); //Enviem el nom del producte amb la id Producte
        editor.commit();

        Intent changeActivity = new Intent(getApplicationContext(), ProductActivity.class);
        startActivity(changeActivity);
    }
}
