package com.example.joell.canieatit.Activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.joell.canieatit.GestorListView.EntityListMarcas;
import com.example.joell.canieatit.GestorListView.EntityListProductos;
import com.example.joell.canieatit.GestorListView.MarcasListAdapter;
import com.example.joell.canieatit.GestorListView.ProductosListAdapter;
import com.example.joell.canieatit.R;

import java.util.ArrayList;
import java.util.List;

public class MarcaActivityNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {

    String nombreProducto;
    List<String> Marcas = new ArrayList<String>();
    SQLiteDatabase mydatabase;
    private static final String TAG = "MainActivity";

    ListView mListView, mListView2;

    SharedPreferences sf;

    // Lista
    ArrayList<EntityListMarcas> listItemsMarcas = new ArrayList<>();
    ArrayList<EntityListProductos> listItemsProductos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marca_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Marcas");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        buscarBBDDMarcas();
        inicialitzarLV();
    }

    private void inicialitzarLV(){
        // Inicialitzar i omplir ListView
        Log.d(TAG, "onCreate: Started.");
        mListView = (ListView) findViewById(R.id.LV_Marcas);
        MarcasListAdapter adapter = new MarcasListAdapter(this, R.layout.item_productos2, listItemsMarcas);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
    }

    private void inicialitzarLV2(){
        buscarBBDDProductosMarcas();

        mListView2 = (ListView) findViewById(R.id.LV_Productos_Marca);
        ProductosListAdapter adapter2 = new ProductosListAdapter(this, R.layout.item_productos2, listItemsProductos);
        mListView2.setAdapter(adapter2);
        mListView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EntityListProductos item = (EntityListProductos) parent.getItemAtPosition(position);
                int Producto = item.getId();

                //Enviem la ID del producte per reutilitzar-la a la següent activity
                sf=PreferenceManager.getDefaultSharedPreferences(MarcaActivityNav.this);
                SharedPreferences.Editor editor = sf.edit();
                editor.putInt("Producto", Producto); //Enviem el nom del producte amb la id Producte
                editor.commit();

                Intent changeActivity = new Intent(getApplicationContext(), ProductActivity.class);
                startActivity(changeActivity);
            }
        });

    }

    private void buscarBBDDProductosMarcas() {
        // SELECT PER OBTINDRE ELS PRODUCTS DE LES MARQUES
        String sqlQuery2 = "SELECT * FROM productos P LEFT JOIN marcas M ON M._id = P.idmarca WHERE M.nombre = '" + nombreProducto + "' AND P._id IN (SELECT U.idProducto FROM union_ U WHERE U.idAlergeno IN (SELECT A._id FROM alergenos_user A WHERE A.tiene = 'false'));";

       // SELECT * FROM productos WHERE _id IN (SELECT idProducto FROM union_ WHERE idAlergeno IN (SELECT _id FROM alergenos_user WHERE tiene = 'false')) AND _id <> "+idProducto+";";
        Cursor resultSet2 = this.mydatabase.rawQuery(sqlQuery2, null);
        resultSet2.moveToFirst();
        int id2 = 0;
        String nombre2 = null;
        int a = resultSet2.getCount();
        listItemsProductos.clear();

     /*   do {
            id2 = resultSet2.getInt(1);
            nombre2 = resultSet2.getString(2);

            listItemsProductos.add(new EntityListProductos(nombre2, id2));
        } while (resultSet2.moveToNext()); */

        int str;
        try{
            str = resultSet2.getInt(1);
        }catch (Exception e){
            str = 0;
        }

        if(str != 0) {
            do{
                id2 = resultSet2.getInt(1);
                nombre2 = resultSet2.getString(2);

                listItemsProductos.add(new EntityListProductos(nombre2, id2));
            }while (resultSet2.moveToNext());
        }else{
            Toast.makeText(this,"No encontramos alternativas", Toast.LENGTH_SHORT).show();
            listItemsProductos.add(new EntityListProductos("No encontramos alternativas",-1)); // TODO Potser aquest -1 genera error
        }

    }

    private void buscarBBDDMarcas(){


        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);



        // SELECT PER OBTINDRE LES MARQUES
        String sqlQuery = "SELECT _id, nombre FROM marcas";

        Cursor resultSet = this.mydatabase.rawQuery(sqlQuery, null);
        resultSet.moveToFirst(); // Movem el cursor al primer resultat
        int id = 0;
        String nombre = null;

        do{
            id = resultSet.getInt(0);
            nombre = resultSet.getString(1);

            listItemsMarcas.add(new EntityListMarcas(nombre, id));

        }while (resultSet.moveToNext());




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.marca_activity_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_consultar_marca) {
            Intent ChangeActivity = new Intent(getApplicationContext(), ConsultaActivityNav.class);
            startActivity(ChangeActivity);
        } else if (id == R.id.nav_perfil_marca) {
            Intent ChangeActivity = new Intent(getApplicationContext(), PerfilActivityNav.class);
            startActivity(ChangeActivity);
        }else if ( id == R.id.nav_marcas_marca){
            Intent ChangeActivity = new Intent(getApplicationContext(), MarcaActivityNav.class);
            startActivity(ChangeActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EntityListMarcas item = (EntityListMarcas) parent.getItemAtPosition(position);
        nombreProducto = item.getNombre();

        inicialitzarLV2();
    }
}
