package com.example.joell.canieatit.classes;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name="marca")
public class Marcas {
    @Element(name="id")
    private int id;
    @Element(name="nombre")
    private String nombre;

    public Marcas() {
    }

    public Marcas(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
