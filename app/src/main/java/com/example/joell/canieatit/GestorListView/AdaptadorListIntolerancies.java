package com.example.joell.canieatit.GestorListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.joell.canieatit.R;

import java.util.ArrayList;

public class AdaptadorListIntolerancies extends BaseAdapter {
    private Context context;
    private ArrayList<EntityListIntolerancies> listIntolerancies;

    public AdaptadorListIntolerancies(Context context, ArrayList<EntityListIntolerancies> listIntolerancies) {
        this.context = context;
        this.listIntolerancies = listIntolerancies;
    }

    @Override
    public int getCount() {
        return listIntolerancies.size();
    }

    @Override
    public Object getItem(int position) {
        return listIntolerancies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EntityListIntolerancies Item = (EntityListIntolerancies) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.item_alergenos, null);
        ImageView imgFoto = (ImageView) convertView.findViewById(R.id.LogoInt);
        TextView tvTitulo = (TextView) convertView.findViewById(R.id.tvIntolerancia);

        imgFoto.setImageResource(Item.getImgFoto());
        tvTitulo.setText(Item.getTitulo());

        return convertView;

    }
}
