package com.example.joell.canieatit.Activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.joell.canieatit.GestorListView.EntityListProductos;
import com.example.joell.canieatit.R;

public class LoginActivity extends AppCompatActivity {

    private SQLiteDatabase mydatabase;
    SharedPreferences sf;
    EditText user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

         user = (EditText) findViewById(R.id.NomUsuari);
         sf=PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

         CrearBBDD();

         //Desactivar les següents 3 linies de codi un cop estigui finalitzat la apicació
        /* SharedPreferences.Editor editor = sf.edit();
         editor.putBoolean("Loged", false);
         editor.commit(); */

         Boolean loged = sf.getBoolean("Loged", false);



         if(!loged) { // Si no està loguejat, mostrarà aquesta activity i habilitarà el botó per accedir

             CardView CV = (CardView) findViewById(R.id.Acceder);
             CV.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     String User = user.getText().toString();
                     SharedPreferences.Editor editor = sf.edit();
                     editor.putString("User", User); //Enviarà el nom d'usuari a les demes activitys
                     editor.putBoolean("Loged", true); //Indicarà que l'usuari ja està loguejat
                     editor.commit();



                     Intent MainActivityNav = new Intent(getApplicationContext(), MainActivityNav.class);
                     startActivity(MainActivityNav);
                 }
             });
         }else{ //Si ja està loguejat accedirà directament al SplashActivity

             Intent SplashActivity = new Intent(getApplicationContext(), SplashActivity.class);
             startActivity(SplashActivity);

         }

    }

    private void CrearBBDD(){
        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);
        //SI LA TAULA PERFIL NO EXISTEIX LA CREEM
        String sqlCreate = "CREATE TABLE IF NOT EXISTS perfil " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                " usuari TEXT)";
        this.mydatabase.execSQL(sqlCreate);
    }

    private void InsertarProductos(){
        //INSERT DE LOS PRODUCTOS DEL XML

        String sqlQuery = "INSERT INTO perfil (usuari) VALUES ('"  + user +  "');";
        this.mydatabase.execSQL(sqlQuery);

    }
}
