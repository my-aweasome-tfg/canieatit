package com.example.joell.canieatit.classes;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="producto")
public class Productos {
    @Element(name="id")
    private int id;
    @Element(name="idMarca")
    private int idMarca;
    @Element(name="nombre")
    private String nombre;
    @Element(name="descripcion")
    private String descripcion;

    public Productos() {
    }

    public Productos(int id, int idMarca, String nombre, String descripcion) {
        this.id = id;
        this.idMarca = idMarca;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
