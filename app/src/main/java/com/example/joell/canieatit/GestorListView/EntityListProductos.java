package com.example.joell.canieatit.GestorListView;

public class EntityListProductos {

    private String nombre;
    private int id;


    public EntityListProductos(String nombre, int id) {
        this.nombre = nombre;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }
}
