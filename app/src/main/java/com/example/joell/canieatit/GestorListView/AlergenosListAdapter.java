package com.example.joell.canieatit.GestorListView;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.joell.canieatit.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

import static android.database.sqlite.SQLiteDatabase.openDatabase;
import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

public class AlergenosListAdapter  extends ArrayAdapter<EntityListIntolerancies> {

    private static final String TAG = "AlergenosListAdapter";

    private Context mContext;
    private  int mResource;
    private  int lastPosition = -1;
    private SQLiteDatabase mydatabase;
    int id;
    ArrayList<EntityListIntolerancies> Objects;

    /**
     * Holds variables in a View
     */
    private static class  ViewHolder{
        TextView name;
        ImageView img;
        CheckBox checkBox;


    }

    public AlergenosListAdapter(Context context, int resource, ArrayList<EntityListIntolerancies> objects){
        super(context,resource,objects);
        Objects = objects;
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       //Setup image loader
        setupImageLoader();

        //get the persons information
        String titulo = getItem(position).getTitulo();
        String imgURL = getItem(position).getImgURL();
        id = getItem(position).getId();
        boolean tiene;

         //Create the person object with the information
         EntityListIntolerancies person = new EntityListIntolerancies(titulo,imgURL,id);

        //create the view result for showing the animation
        final View result;

        //ViewHolder object
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);
            holder= new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.tvIntolerancia);
            holder.img = (ImageView) convertView.findViewById(R.id.LogoInt);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.check_perfil);

            result = convertView;

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        ImageLoader imageLoader = ImageLoader.getInstance();

        int defaultImage = mContext.getResources().getIdentifier("@drwable/image_failed",null,mContext.getPackageName());

        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(defaultImage)
                .showImageOnFail(defaultImage)
                .showImageOnLoading(defaultImage).build();


        imageLoader.displayImage(imgURL, holder.img, options);

        holder.name.setText(person.getTitulo());
       if (Objects.get(position).isTiene()) {
            holder.checkBox.setChecked(true);
        }else {
            holder.checkBox.setChecked(false);
        }

        //holder.checkBox.setChecked(false);








        return convertView;
    }



    private void setupImageLoader(){
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                mContext)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }


}
