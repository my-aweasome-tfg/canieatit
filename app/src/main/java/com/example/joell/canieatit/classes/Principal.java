package com.example.joell.canieatit.classes;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name="root")
public class Principal {


    @Path("alergenos")
    @ElementList(required = true, inline = true)
    private ArrayList<Alergenos> alergenos;
    @Path("marcas")
    @ElementList(required = true,inline = true)
    private ArrayList<Marcas> marcas;
    @Path("productos")
    @ElementList(required = true, inline = true)
    private ArrayList<Productos> productos;
    @Path("union_alerg")
    @ElementList(required = true, inline = true)
    private ArrayList<Union_alergs> union_alergs;

    public Principal() {
        this.alergenos = new ArrayList<Alergenos>();
        this.productos = new ArrayList<Productos>();
        this.marcas = new ArrayList<Marcas>();
        this.union_alergs= new ArrayList<Union_alergs>();
    }

    public ArrayList<Alergenos> getAlergenos() {
        return alergenos;
    }

    public void setAlergenos(ArrayList<Alergenos> alergenos) {
        this.alergenos = alergenos;
    }

    public ArrayList<Marcas> getMarcas() {
        return marcas;
    }

    public void setMarcas(ArrayList<Marcas> marcas) {
        this.marcas = marcas;
    }

    public ArrayList<Productos> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Productos> productos) {
        this.productos = productos;
    }

    public ArrayList<Union_alergs> getUnion_alergs() {
        return union_alergs;
    }

    public void setUnion_alergs(ArrayList<Union_alergs> union_alergs) {
        this.union_alergs = union_alergs;
    }

    @Override
    public String toString() {
        String str = this.alergenos.get(0).getNombre();

        return str;
    }
}
