package com.example.joell.canieatit.Activitys;

import android.Manifest;
import android.app.DownloadManager;
import android.app.backup.BackupDataInputStream;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.example.joell.canieatit.GestorListView.EntityListIntolerancies;
import com.example.joell.canieatit.GestorListView.EntityListProductos;
import com.example.joell.canieatit.R;
import com.example.joell.canieatit.classes.Alergenos;
import com.example.joell.canieatit.classes.Marcas;
import com.example.joell.canieatit.classes.Principal;
import com.example.joell.canieatit.classes.Productos;
import com.example.joell.canieatit.classes.Union_alergs;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class MainActivityNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Variables globales
    private SQLiteDatabase mydatabase;
    Principal principal = null;
    private static final String TAG = "MainActivity";


    //LISTAS
    private List<String> AlergenosList = new ArrayList<>();
    private List<Integer> AlergenosID = new ArrayList<>();
    private List<String> AlergenosNombre = new ArrayList<>();
    private List<String> AlergenosDescripcion = new ArrayList<>();
    private List<String> AlergenosImg = new ArrayList<>();

   //LISTAS PRODUCTOS
    private List<String> ProductosNombre = new ArrayList<>();
    private List<Integer> ProductosId = new ArrayList<>();
    private List<Integer> ProductosIdMarca = new ArrayList<>();
    private List<String> ProductosDescripcion = new ArrayList<>();

    //LISTAS UNION
    private List<Integer> IDProductoList = new ArrayList<>();
    private List<Integer> IDAlergenoList = new ArrayList<>();

    //LISTAS MARCA
    private List<Integer> IDMarcaList = new ArrayList<>();
    private List<String> NombreMarcaList = new ArrayList<>();

    //LISTAS PARA ListView
    ArrayList<EntityListIntolerancies> listItemsAlergenos = new ArrayList<>();
    ArrayList<EntityListProductos> listItemsProductos = new ArrayList<>();


    SharedPreferences sf;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menú");
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);





        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);





        DownloadFromUrl();
        CrearBBDD();
        eliminarTaula();
        llegirXML();
        InsertarProductos();
        InsertarAlergenosUser();
        InsertarAlergenos();
        InsertarUnion();
        InsertarMarcas();


        Button btnConsulta = (Button) findViewById(R.id.BtnConsulta);
        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ConsultaActivity = new Intent(getApplicationContext(), ConsultaActivityNav.class);
                startActivity(ConsultaActivity);
            }
        });

        Button btnPerfil = (Button) findViewById(R.id.BtnPerfil);
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent PerfilActivity = new Intent(getApplicationContext(), PerfilActivityNav.class);
                startActivity(PerfilActivity);
            }
        });

        Button btnMarca = (Button) findViewById(R.id.BtnMarca);
        btnMarca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MarcaActivity = new Intent(getApplicationContext(), MarcaActivityNav.class);
                startActivity(MarcaActivity);
            }
        });

    }

    private void DownloadFromUrl(){
        String DownloadUrl = "https://canieatit.000webhostapp.com/PlantillaXML2.xml";
        try{
            File root = android.os.Environment.getExternalStorageDirectory();

            File dir = new File(root.getAbsolutePath()+"/XMLS");
            if(dir.exists()==false){
                dir.mkdir();
            }

            URL url = new URL(DownloadUrl);
            File file = new File(dir,"PlantillaXML");
            //boolean deleted = file.delete();


            long startTime = System.currentTimeMillis();
            Log.d("DownloadManager", "download begining");
            Log.d("DownloadManager", "download url: "+url);
            Log.d("DownloadManager", "download file name: PlantillaXML");

            /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();

            /*
             * Define InputStreams to read from the URLConnection.
             */
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            /*
             * Read bytes to the Buffer until there is nothing more to read(-1).
             */

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] data = new byte[5000];
            int current = 0;
            while ((current = bis.read(data,0,data.length)) != -1) {
                buffer.write(data,0,current);
            }

            /* Convert the Bytes read to a String. */
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(buffer.toByteArray());
            fos.flush();
            fos.close();
            Log.d("DownloadManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");



        }catch (IOException e){
            Log.d("DownloadManager", "Error: " + e);
        }
    }


    private void eliminarTaula(){
        String sqlCreate = "DROP TABLE productos";
        this.mydatabase.execSQL(sqlCreate);

        String sqlCreate2 = "CREATE TABLE IF NOT EXISTS productos "+
                "(_id INTEGER PRIMARY KEY, "+
                " idMarca NUMBER, nombre TEXT, descripcion TEXT)";
        this.mydatabase.execSQL(sqlCreate2);

        String sqlCreate3 = "DROP TABLE alergenos";
        this.mydatabase.execSQL(sqlCreate3);

        String sqlCreate4 = "CREATE TABLE IF NOT EXISTS alergenos"+
                "(_id INTEGER PRIMARY KEY,"+
                " nombre TEXT, descripcion TEXT, img TEXT)";
        this.mydatabase.execSQL(sqlCreate4);

        String sqlCreate5 = "DROP TABLE union_";
        this.mydatabase.execSQL(sqlCreate5);

        String sqlCreate6 = "CREATE TABLE IF NOT EXISTS union_"+
                "(idProducto INTEGER PRIMARY KEY,"+
                " idAlergeno NUMBER)";
        this.mydatabase.execSQL(sqlCreate6);

        String sqlCreate7 = "DROP TABLE marcas";
        this.mydatabase.execSQL(sqlCreate7);

        String sqlCreate8 = "CREATE TABLE IF NOT EXISTS marcas"+
                "(_id INTEGER PRIMARY KEY,"+
                " nombre TEXT)";
        this.mydatabase.execSQL(sqlCreate8);


    }

    private void CrearBBDD(){
        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);
        //SI LA TAULA PERFIL NO EXISTEIX LA CREEM
        String sqlCreate = "CREATE TABLE IF NOT EXISTS perfil " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                " usuari TEXT)";
        this.mydatabase.execSQL(sqlCreate);

        //SI LA TAULA PRODUCTOS NO EXISTEIX LA CREEM
        String sqlCreate2 = "CREATE TABLE IF NOT EXISTS productos "+
                "(_id INTEGER PRIMARY KEY, "+
                " idMarca NUMBER, nombre TEXT, descripcion TEXT)";
        this.mydatabase.execSQL(sqlCreate2);

        //SI LA TAULA AlergenosUser NO EXISTEIX LA CREEM
        String sqlCreate3 = "CREATE TABLE IF NOT EXISTS alergenos_user"+
                "(_id INTEGER PRIMARY KEY,"+
                " tiene TEXT)";
        this.mydatabase.execSQL(sqlCreate3);

        //SI LA TAULA ALERGENOS NO EXISTEIX LA CREEM
        String sqlCreate4 = "CREATE TABLE IF NOT EXISTS alergenos"+
                "(_id INTEGER PRIMARY KEY,"+
                " nombre TEXT, descripcion TEXT, img TEXT)";
        this.mydatabase.execSQL(sqlCreate4);

        //SI LA TAULA ALERGENOS NO EXISTEIX LA CREEM
        String sqlCreate5 = "CREATE TABLE IF NOT EXISTS union_"+
                "(idProducto INTEGER PRIMARY KEY,"+
                " idAlergeno NUMBER)";
        this.mydatabase.execSQL(sqlCreate5);

        //SI LA TAULA ALERGENOS NO EXISTEIX LA CREEM
        String sqlCreate6 = "CREATE TABLE IF NOT EXISTS marcas"+
                "(_id INTEGER PRIMARY KEY,"+
                " nombre TEXT)";
        this.mydatabase.execSQL(sqlCreate6);

    }



    private void InsertarAlergenosUser(){
        int id;
        boolean tiene=false;
        sf=PreferenceManager.getDefaultSharedPreferences(MainActivityNav.this);
        SharedPreferences.Editor editor = sf.edit();
        boolean creat = sf.getBoolean("creat", false);
        int count=sf.getInt("Contador", 0);// SHARED preference COUNT

        //EN CASO DE QUE YA ESTEE CREADA LA TABLA, NO SE EJECUTARA EL CODIGO
        if(creat = false) {
            for (int i = 0; i < AlergenosID.size(); i++) {
                id = AlergenosID.get(i);
                String sqlQuery = "INSERT INTO alergenos_user (_id, tiene) VALUES ('" +id+"','"+tiene+"');";
                count++;
            }
            editor.putBoolean("creat", true);
            editor.putInt("Contador",count);
        }

        //EN CASO DE QUE SE AÑADA UN NUEVO ALERGENO DES DE LA APLICACION DE ESCRITORIO
        //SE AÑADIRA EL REGISTRO EN LA TABLA alergenos_user
        if(count > AlergenosID.size()){

            for (int i = count; i<AlergenosID.size(); i++){
                id = AlergenosID.get(i);
                String sqlQuery = "INSERT INTO alergenos_user (_id, tiene) VALUES ('" +id+"','"+tiene+"');";
                count++;
            }
            editor.putInt("Contador",count);
        }

    }

    private void InsertarProductos(){
        //INSERT DE LOS PRODUCTOS DEL XML
        int id, idMarca = 0;
        String nombre, descripcion = null;

        for (int i = 0; i<ProductosNombre.size(); i++) {
            id = ProductosId.get(i);
            idMarca = ProductosIdMarca.get(i);
            nombre = ProductosNombre.get(i);
            descripcion = ProductosDescripcion.get(i);

            String sqlQuery = "INSERT INTO productos (_id, idMarca, nombre, descripcion) VALUES ('" + id + "','" + idMarca + "','" + nombre + "','" + descripcion + "');";
            this.mydatabase.execSQL(sqlQuery);

        }
    }

    private void InsertarMarcas(){
        //INSERT DE LAS MARCAS DEL XML
        int id = 0;
        String nombre = null;

        for (int i = 0; i<NombreMarcaList.size();i++){
            id = IDMarcaList.get(i);
            nombre = NombreMarcaList.get(i);

            String sqlQuery = "INSERT INTO marcas (_id, nombre) VALUES ('" + id + "','"+nombre+"');";
            this.mydatabase.execSQL(sqlQuery);
        }
    }

    private void InsertarAlergenos(){
        //INSERT DE LOS PRODUCTOS DEL XML
        int id=0;
        String nombre, descripcion, img = null;

        for (int i = 0; i<AlergenosNombre.size(); i++){
            id = AlergenosID.get(i);
            nombre = AlergenosNombre.get(i);
            descripcion = AlergenosDescripcion.get(i);
            img = AlergenosImg.get(i);

            String sqlQuery = "INSERT INTO alergenos (_id, nombre, descripcion, img) VALUES ('"+id+"','"+nombre+"','"+descripcion+"','"+img+"');";
            this.mydatabase.execSQL(sqlQuery);
        }
    }

    private void InsertarUnion(){
        int idAlergeno, idProducto;

        for (int i = 0; i<IDAlergenoList.size(); i++){
            idAlergeno = IDAlergenoList.get(i);
            idProducto = IDProductoList.get(i);

            String sqlQuery = "INSERT INTO union_ (idProducto, idAlergeno) VALUES ('"+idProducto+"','"+idAlergeno+"');";
            this.mydatabase.execSQL(sqlQuery);
        }
    }

     private void llegirXML() {
        AssetManager am = this.getAssets();
        InputStream input = null;

        /*try {
            input = am.open("PlantillaXML.xml");
        } catch (IOException e) {
            e.printStackTrace(); */

        String fileName="XMLS/PlantillaXML";

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
        Serializer ser = new Persister();

        try {
            principal = ser.read(Principal.class, file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Obtenir una llista de noms de intolerancies
        try {
            ArrayList<Alergenos> alergenos = principal.getAlergenos();

            for (int i = 0; i < alergenos.size(); i++) {
                AlergenosList.add(alergenos.get(i).getNombre());
                AlergenosList.add(alergenos.get(i).getImg());
                AlergenosList.add(Integer.toString(alergenos.get(i).getId()));

                //Forma nova afegir imatge
                String imgURL = "assets://" + alergenos.get(i).getImg();
                listItemsAlergenos.add(new EntityListIntolerancies(alergenos.get(i).getNombre(), imgURL, alergenos.get(i).getId()));

                // Omplim ArrayList AlergenosID per poder omplir la taula AlergenosUser
                AlergenosID.add(alergenos.get(i).getId());



            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Obtenir llista de productes
        try{
            ArrayList<Productos> productos = principal.getProductos();

            for (int i = 0; i<productos.size(); i++){
                ProductosId.add(productos.get(i).getId());
                ProductosIdMarca.add(productos.get(i).getIdMarca());
                ProductosNombre.add(productos.get(i).getNombre());
                ProductosDescripcion.add(productos.get(i).getDescripcion());



                }
        }catch (Exception e){
            e.printStackTrace();
        }

        //Obtenir llista de alergenos
         try{
             ArrayList<Alergenos> alergenos = principal.getAlergenos();

             for (int i = 0; i<alergenos.size(); i++){
                 AlergenosID.add(alergenos.get(i).getId());
                 AlergenosNombre.add(alergenos.get(i).getNombre());
                 AlergenosDescripcion.add(alergenos.get(i).getDescripcion());
                 AlergenosImg.add(alergenos.get(i).getImg());



             }
         }catch (Exception e){
             e.printStackTrace();
         }

         //Obtenir llista de union
         try{
            ArrayList<Union_alergs> union = principal.getUnion_alergs();

            for (int i = 0; i<union.size(); i++){
                IDAlergenoList.add(union.get(i).getIdAlergeno());
                IDProductoList.add(union.get(i).getIdProducto());
            }
         }catch (Exception e){
            e.printStackTrace();
         }

         //Obtenir llista de marcas
         try{
            ArrayList<Marcas> marca = principal.getMarcas();

            for (int i = 0; i<marca.size(); i++){
                IDMarcaList.add(marca.get(i).getId());
                NombreMarcaList.add(marca.get(i).getNombre());
            }
         }catch (Exception e){
            e.printStackTrace();
         }
    }





    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item_alergenos clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item_alergenos clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_consultar) {
            Intent ChangeActivity = new Intent(getApplicationContext(), ConsultaActivityNav.class);
            startActivity(ChangeActivity);
        } else if (id == R.id.nav_perfil) {
            Intent ChangeActivity = new Intent(getApplicationContext(), PerfilActivityNav.class);
            startActivity(ChangeActivity);
        }else if ( id == R.id.nav_marca){
            Intent ChangeActivity = new Intent(getApplicationContext(), MarcaActivityNav.class);
            startActivity(ChangeActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
