package com.example.joell.canieatit.classes;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="alergeno")
public class Alergenos {

    @Element(name = "id")
    private int id;
    @Element(name = "nombre")
    private String nombre;
    @Element(name = "descripcion")
    private String descripcion;
    @Element(name = "img")
    private String img;

    public Alergenos() {
    }

    public Alergenos(int id, String nombre, String descripcion, String img) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
