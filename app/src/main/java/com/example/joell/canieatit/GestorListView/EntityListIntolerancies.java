package com.example.joell.canieatit.GestorListView;

import android.net.Uri;
import android.widget.ImageView;

public class EntityListIntolerancies {

    private int imgFoto;
    private String titulo;
    private String imgURL;
    private int id;
    private boolean tiene;


    public EntityListIntolerancies(String titulo, String imgURL, int id, boolean tiene) {
        this.titulo = titulo;
        this.imgURL = imgURL;
        this.id = id;
        this.tiene = tiene;
    }

    public EntityListIntolerancies(String titulo, String imgURL, int id) {
        this.titulo = titulo;
        this.imgURL = imgURL;
        this.id = id;
    }

    
    public int getImgFoto() {
        return imgFoto;
    }

    public String getTitulo() {
        return titulo;
    }


    public String getImgURL() {
        return imgURL;
    }

    public int getId() {
        return id;
    }

    public boolean isTiene() {
        return tiene;
    }

    public void setImgFoto(int imgFoto) {
        this.imgFoto = imgFoto;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTiene(boolean tiene) {
        this.tiene = tiene;
    }
}
