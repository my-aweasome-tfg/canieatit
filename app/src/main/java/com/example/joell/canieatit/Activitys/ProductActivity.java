package com.example.joell.canieatit.Activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.joell.canieatit.GestorListView.AlergenosListAdapter;
import com.example.joell.canieatit.GestorListView.AlergenosListAdapterProduct;
import com.example.joell.canieatit.GestorListView.EntityListIntolerancies;
import com.example.joell.canieatit.GestorListView.EntityListProductos;
import com.example.joell.canieatit.GestorListView.ProductosListAdapter;
import com.example.joell.canieatit.R;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    SharedPreferences sf;
    int idProducto;
    ListView mListView, mListView2;
    private static final String TAG = "MainActivity";

    private SQLiteDatabase mydatabase;

    // Lista
    ArrayList<EntityListIntolerancies> listItemsAlergenos = new ArrayList<>();
    ArrayList<EntityListProductos> listItemsProductos = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Productos");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_product);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sf=PreferenceManager.getDefaultSharedPreferences(this);
        idProducto = sf.getInt("Producto",0);

        buscarBBDD(); // SELECT del producto

        // Inicialitzar i omplir ListView
        Log.d(TAG, "onCreate: Started.");
        mListView = (ListView) findViewById(R.id.LV_Llista_Alergeno);
        AlergenosListAdapterProduct adapter = new AlergenosListAdapterProduct(this, R.layout.item_alergenos_product, listItemsAlergenos);
        mListView.setAdapter(adapter);

        mListView2 = (ListView) findViewById(R.id.LV_Llista_Alternativas);
        ProductosListAdapter adapter2 = new ProductosListAdapter(this, R.layout.item_productos2, listItemsProductos);
        mListView2.setAdapter(adapter2);
        mListView2.setOnItemClickListener(this);

        /* mListView.setItemsCanFocus(true);
        mListView2.setItemsCanFocus(true);*/

    }

    private void buscarBBDD(){
        int id;
        String nombre;
        String img;
        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);

        // SELECT PER OBTINDRE EL PRODUCT
        String sqlQuery = "SELECT * FROM alergenos WHERE _id IN (" +
                "SELECT idAlergeno FROM union_ WHERE idProducto = "+idProducto+");";

        Cursor resultSet = this.mydatabase.rawQuery(sqlQuery, null);
        resultSet.moveToFirst(); // Movem el cursor al primer resultat

        do{
            id = resultSet.getInt(0);
            nombre = resultSet.getString(1);
            img = resultSet.getString(3);

            String imgURL = "assets://" +img;
            listItemsAlergenos.add(new EntityListIntolerancies(nombre, imgURL, id));

        }while (resultSet.moveToNext());

        // SELECT PER OBTINDRE ALTERNATIVES
        /*String sqlQuery2 = "SELECT * FROM productos WHERE _id NOT IN ("+
                "SELECT idProducto FROM union_ WHERE idAlergeno IN("+
                "SELECT _id FROM alergenos WHERE _id  IN("+
                "SELECT idAlergeno FROM union_ WHERE idProducto = "+idProducto+")))"; */
        String sqlQuery2 = "SELECT * FROM productos WHERE _id IN (SELECT idProducto FROM union_ WHERE idAlergeno IN (SELECT _id FROM alergenos_user WHERE tiene = 'false')) AND _id <> "+idProducto+";";

        Cursor resultSet2 = this.mydatabase.rawQuery(sqlQuery2,null);
        resultSet2.moveToFirst();

        // Amb aquest codi ens valida si hi ha dades al SELECT, en cas de no haver-hi ens mostrarà
        // un missatge d'error i s'omplirà la list view amb un missatge de falta de dades. En cas
        // de tenir dades ens omplirà la list view amb les dades pertinents del select
        String str;
        try{
            str = resultSet2.getString(2);
        }catch (Exception e){
            str = "";
        }

        if(!str.equals("")) {
            do{
                nombre = resultSet2.getString(2);
                id = resultSet2.getInt(0);
                listItemsProductos.add(new EntityListProductos(nombre,id));
            }while (resultSet2.moveToNext());
        }else{
            Toast.makeText(this,"No encontramos alternativas", Toast.LENGTH_SHORT).show();
            listItemsProductos.add(new EntityListProductos("No encontramos alternativas",-1)); // TODO Potser aquest -1 genera error
        }


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EntityListProductos item = (EntityListProductos) parent.getItemAtPosition(position);
        int Producto = item.getId();

        //Enviem la ID del producte per reutilitzar-la a la següent activity
        sf=PreferenceManager.getDefaultSharedPreferences(ProductActivity.this);
        SharedPreferences.Editor editor = sf.edit();
        editor.putInt("Producto", Producto); //Enviem el nom del producte amb la id Producte
        editor.commit();

        Intent changeActivity = new Intent(getApplicationContext(), ProductActivity.class);
        startActivity(changeActivity);
        finish();
    }
}
