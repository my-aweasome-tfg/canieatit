package com.example.joell.canieatit.classes;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="union")
public class Union_alergs {

    @Element(name="id_product")
    private  int idProducto;
    @Element(name="id_alerg")
    private int idAlergeno;

    public Union_alergs() {
    }

    public Union_alergs(int idProducto, int idAlergeno) {
        this.idProducto = idProducto;
        this.idAlergeno = idAlergeno;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdAlergeno() {
        return idAlergeno;
    }

    public void setIdAlergeno(int idAlergeno) {
        this.idAlergeno = idAlergeno;
    }
}
