package com.example.joell.canieatit.Activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.joell.canieatit.GestorListView.AlergenosListAdapter;
import com.example.joell.canieatit.GestorListView.EntityListIntolerancies;
import com.example.joell.canieatit.GestorListView.EntityListProductos;
import com.example.joell.canieatit.R;
import com.example.joell.canieatit.classes.Alergenos;
import com.example.joell.canieatit.classes.Principal;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PerfilActivityNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener{

    private static final String TAG = "MainActivity";
    ArrayList<EntityListIntolerancies> listItemsAlergenos = new ArrayList<>();
    private List<String> AlergenosList = new ArrayList<>();
    Principal principal = null;
    private SQLiteDatabase mydatabase;
    SharedPreferences sf;
    String user;

    CheckBox checkBox;
    ListView mListView;
    int titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Perfil");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        CrearBBDD();
        llegirXML();
        inicializarListView();

        sf=PreferenceManager.getDefaultSharedPreferences(PerfilActivityNav.this);
        user = sf.getString("User","Null");
        TextView ETNom = (TextView) findViewById(R.id.ET_NomUsuari);
        ETNom.setText(user);



    }

    private void inicializarListView(){
        Log.d(TAG, "onCreate: Started.");
        mListView = (ListView) findViewById(R.id.LV_Intolerancias);
        mListView.setItemsCanFocus(true);
        AlergenosListAdapter adapter = new AlergenosListAdapter(this, R.layout.item_alergenos, listItemsAlergenos);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
    }


    private void updateBBDD(boolean checked) {

        try {
            if (checked == false) {
                String sqlQuery = "UPDATE alergenos_user set tiene = 'true' WHERE _id = " + titulo + ";";
                this.mydatabase.execSQL(sqlQuery);

            } else {
                String sqlQuery = "UPDATE alergenos_user set tiene = 'false' WHERE _id = " + titulo + ";";
                this.mydatabase.execSQL(sqlQuery);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buscarBBDD(){
        int id;
        String nombre;
        String img;
        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);

        // SELECT PER OBTINDRE ELS ALERGENS
        String sqlQuery = "SELECT * FROM alergenos;";

        Cursor resultSet = this.mydatabase.rawQuery(sqlQuery, null);
        resultSet.moveToFirst(); // Movem el cursor al primer resultat

        do{
            id = resultSet.getInt(0);
            nombre = resultSet.getString(1);
            img = resultSet.getString(3);

            String imgURL = "assets://" +img;
            listItemsAlergenos.add(new EntityListIntolerancies(nombre, imgURL, id));

        }while (resultSet.moveToNext());
    }

    private void CrearBBDD(){
        //SI LA BBDD NO EXISTEIX LA CREEM
        this.mydatabase = this.openOrCreateDatabase("BBDD", MODE_PRIVATE,null);

        //SI LA TAULA AlergenosUser NO EXISTEIX LA CREEM
        String sqlCreate = "CREATE TABLE IF NOT EXISTS alergenos_user"+
                "(_id INTEGER PRIMARY KEY,"+
                " tiene BOOLEAN)";
        this.mydatabase.execSQL(sqlCreate);
    }

    private void inserirBBDD(int id){
        String sqlQuery = "INSERT OR IGNORE INTO alergenos_user (_id, tiene) VALUES ('"+id+"','false');";
        this.mydatabase.execSQL(sqlQuery);
    }

    private void llegirXML() {
        AssetManager am = this.getAssets();
        InputStream input = null;

        /*try {
            input = am.open("PlantillaXML.xml");
        } catch (IOException e) {
            e.printStackTrace(); */

        String fileName="XMLS/PlantillaXML";

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
        Serializer ser = new Persister();

        try {
            principal = ser.read(Principal.class, file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Obtenir una llista de noms de intolerancies
        try {
            ArrayList<Alergenos> alergenos = principal.getAlergenos();

            for (int i = 0; i < alergenos.size(); i++) {
                AlergenosList.add(alergenos.get(i).getNombre());
                AlergenosList.add(alergenos.get(i).getImg());
                inserirBBDD(alergenos.get(i).getId()); // Inserir els estats dels checkbox a fals en cas de ser un nou alergern

                //Forma nova
                String imgURL = "assets://" + alergenos.get(i).getImg();



                //TODO Ha d'anar a la BBDD i comprovar els checkbox

                String sqlQuery = "SELECT tiene FROM alergenos_user WHERE _id = '" + alergenos.get(i).getId() + "';";
                Cursor resultSet = this.mydatabase.rawQuery(sqlQuery, null);
                resultSet.moveToFirst(); // Movem el cursor al primer resultat

                if(resultSet.getString(0).equals("true")) {
                    listItemsAlergenos.add(new EntityListIntolerancies(alergenos.get(i).getNombre(), imgURL, alergenos.get(i).getId(), true));
                }else{
                    listItemsAlergenos.add(new EntityListIntolerancies(alergenos.get(i).getNombre(), imgURL, alergenos.get(i).getId(), false));
                }

                //TODO Finalitzar la prova de la BBDD



            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.perfil_activity_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_consultar_perfil) {
            Intent ChangeActivity = new Intent(getApplicationContext(), ConsultaActivityNav.class);
            startActivity(ChangeActivity);
        } else if (id == R.id.nav_perfil_perfil) {
            Intent ChangeActivity = new Intent(getApplicationContext(), PerfilActivityNav.class);
            startActivity(ChangeActivity);
        }else if ( id == R.id.nav_marcas_perfil){
            Intent ChangeActivity = new Intent(getApplicationContext(), MarcaActivityNav.class);
            startActivity(ChangeActivity);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EntityListIntolerancies item = (EntityListIntolerancies) parent.getItemAtPosition(position);
        titulo = item.getId();


        CheckBox box = (CheckBox)view.findViewById(R.id.check_perfil);
        if(box.isChecked()){
            updateBBDD(true);
            box.setChecked(false);

        }else{
            updateBBDD(false);
            box.setChecked(true);

        }
    }
}
