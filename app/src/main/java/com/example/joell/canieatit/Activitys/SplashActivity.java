package com.example.joell.canieatit.Activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.example.joell.canieatit.R;

public class SplashActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    SharedPreferences sf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sf=PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        String user = sf.getString("User", null);

        EditText ETNom = (EditText) findViewById(R.id.ET_Splash);
        ETNom.setText("¡Que alegria volver a verte "+user+"!");
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {


                Intent i = new Intent(SplashActivity.this, MainActivityNav.class);
                startActivity(i);
                finish();


            }
        },4000); // 4 seconds
    }
}


/*

Intent i = new Intent(SplashActivity.this, MainActivityNav.class);
                startActivity(i);
                finish();

    sf=PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        String user = sf.getString("User", null);

        EditText ETNom = (EditText) findViewById(R.id.ET_Splash);
        ETNom.setText("¡Que alegria volver a verte "+user);

 */